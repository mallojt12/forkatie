  # @ job_type = serial
  # @ class = NORMAL
  # @ account_no = NONE
  #PBS -m e -l vmem=100gb,walltime=16:00:00
  # @ notification = always
  # @ output = batch.$(cluster).out
  # @ error = batch.$(Cluster).err
  # @ queue


R CMD BATCH ~/runRNAeug.R ~/rnaEugOut_2014Jan21_snvs.out
R CMD BATCH ~/codonBiasEug.R  ~/eugCodon_2014Jan23.out

